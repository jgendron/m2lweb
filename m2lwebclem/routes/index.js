var express = require('express');
var router = express.Router();

var controllerIndex = require("../controllers/controllerIndex");
var controllerActivite = require("../controllers/controllerActivite");
var controllerProfil = require("../controllers/controllerProfile");
var controllerJuju = require("../controllers/controllerSeance");
var controllerProgrammeSante = require("../controllers/controllerProgrammeSante");

/* GET Connexion page. */
router.get('/', controllerIndex.connexion);
router.post('/', controllerIndex.verificationConnexion);

router.get('/logout', controllerIndex.deconnexion);

router.get('/index', controllerIndex.index);

router.get('/profile', controllerProfil.profileSeniorInfo);

router.get('/programmeSante',controllerProgrammeSante.getInfoProg);

router.post('/programmeSante/inscrire',controllerProgrammeSante.inscriptionAct)
router.post('/programmeSante/retracter',controllerProgrammeSante.retractAct)

/* ACTIVITES*/
router.get('/activites',controllerActivite.getAllActivites);

router.get('/index/:num/activiteInscrit',controllerJuju.getActivitesBySenior);
router.get('/activiteInscrit/:num/seances',controllerJuju.getSeanceByActivite);






module.exports = router;
