const { Client } = require('pg');
const activite = require('../model/activite');

class activitePgDAO {
    constructor() {
    }

    getActivitesBySenior(num, displaycb) {
        console.log(num)
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        });

        client.connect(function (err) {
            if (err) return done(err);
        });

            const query = {
                name: 'fetch-activite-by-senior',
                text: 'SELECT activite.identifiant,activite.designation,activite.nbmax FROM activite INNER JOIN participer ON activite.identifiant = participer.codeactivite JOIN senior ON senior.numsecu = participer.codesenior where senior.numsecu = $1',
                values: [num]
            };

            client.query(query, function (err, result) {
                    let listeDesActivites = [];
                    if (err) {
                        console.log(err.stack);
                    } else {
                        result.rows.forEach(function (row) {
                            let uneActivite;

                            uneActivite = new activite(row['identifiant'], row['designation'], row['nbmax']);
                            listeDesActivites.push(uneActivite);
                        });
                        displaycb(listeDesActivites);

                    }
                    client.end();
                });
        };

    getActivitesDispoBySenior(num, displaycb) {
        console.log(num)
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        });

        client.connect(function (err) {
            if (err) return done(err);
        });

        const query = {
            name: 'fetch-activite-by-senior',
            text: 'SELECT activite.identifiant,activite.designation,activite.nbmax FROM activite\n' +
            'where activite.identifiant not in\n' +
            '(SELECT participer.codeactivite\n' +
            'FROM participer\n' +
            'where participer.codesenior = $1)',
            values: [num]
        };

        client.query(query, function (err, result) {
            let listeDesActivites = [];
            if (err) {
                console.log(err.stack);
            } else {
                result.rows.forEach(function (row) {
                    let uneActivite;

                    uneActivite = new activite(row['identifiant'], row['designation'], row['nbmax']);
                    listeDesActivites.push(uneActivite);
                });
                displaycb(listeDesActivites);

            }
            client.end();
        });
    };
}
module.exports = activitePgDAO;