const { Client } = require('pg');
const seance = require('../model/seance');

class seancePdDAO {

    constructor() {
    }
    getSeanceByActivite(num, displaycb) {
        console.log(num)
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        });

        client.connect(function (err) {
            if (err) return done(err);
        });

        const query = {
            name: 'fetch-activite-by-senior',
            text: 'select * from seance inner join activite on seance.idact = activite.identifiant where activite.identifiant = $1',
            values: [num]
        };

        client.query(query, function (err, result) {
            let listeDesSeances = [];
            if (err) {
                console.log(err.stack);
            } else {
                result.rows.forEach(function (row) {
                    let uneSeance;

                    uneSeance = new seance(row['code'], row['dateseance'], row['idact']);
                    listeDesSeances.push(uneSeance);
                });
                displaycb(listeDesSeances);


            }
            client.end();
        });
    };
}
module.exports = seancePdDAO;