class medecin {

    constructor(unIdentifiant, unNom, unPrenom, unEmail, unTel, uneVille,uneDateNaissance){
        this._unIdentifiant = unIdentifiant;
        this._unNom = unNom;
        this._unPrenom = unPrenom;
        this._unEmail = unEmail;
        this._unTel = unTel;
        this._uneVille = uneVille;
        this._uneDateNaissance = uneDateNaissance;
    }


    get unIdentifiant() {
        return this._unIdentifiant;
    }

    get unNom() {
        return this._unNom;
    }

    get unPrenom() {
        return this._unPrenom;
    }

    get unEmail() {
        return this._unEmail;
    }

    get unTel() {
        return this._unTel;
    }

    get uneVille() {
        return this._uneVille;
    }

    get uneDateNaissance() {
        return this._uneDateNaissance;
    }
}
module.exports = medecin;