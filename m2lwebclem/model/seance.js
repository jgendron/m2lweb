class seance {

    constructor(unCode, uneDateSeance, unIdAct){

        this._unCode = unCode;
        this._uneDateSeance = uneDateSeance;
        this._unIdAct = unIdAct;
    }

    get Code() {
        return this._unCode;
    }

    get DateSeance() {
        return this._uneDateSeance;
    }

    get Nbmax() {
        return this._unIdAct;
    }
}
module.exports = seance;