const {Client} = require('pg');
const senior = require('../model/senior');

class seniorDAO{
    constructor(){
    }

    getSeniorActif(num,displaycb) {
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        })

        client.connect(function (err) {
            if(err) return done(err);
        });

        const query = {
            name: 'fetch-all-senior',
            text: "SELECT * FROM senior WHERE senior.numsecu like $1",
            values: [num]
        }

        client.query(query, function(err,result){
            let listeSenior = [];
            if(err){
                console.log(err.stack);
            }else {
                result.rows.forEach(function(row){
                    let unSenior;
                    unSenior = new senior(row['numsecu'], row['nom'], row['datenaissance'], row['datedeces'], row['email'], row['password']);
                    listeSenior.push(unSenior);
                });
                displaycb(listeSenior);
            }
            client.end();
        });
    }
}
module.exports = seniorDAO;