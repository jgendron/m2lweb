class participer {

    constructor(unCodeSeance, unCodeSenior){

        this._unCodeSeance = unCodeSeance;
        this._unCodeSenior = unCodeSenior;
    }

    get unCodeSeance() {
        return this._unCodeSeance;
    }

    get unCodeSenior() {
        return this._unCodeSenior;
    }

}
module.exports = participer;