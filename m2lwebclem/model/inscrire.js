class inscrire {

    constructor(unCodeSenior, unIdActivite){

        this._unCodeSenior = unCodeSenior;
        this._unIdActivite = unIdActivite;
    }

    get unCodeSenior() {
        return this._unCodeSenior;
    }

    get unIdActivite() {
        return this._unIdActivite;
    }

}
module.exports = inscrire;