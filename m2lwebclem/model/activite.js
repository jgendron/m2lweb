class activite {

    constructor(unIdentifiant, uneDesignation, unNbmax){

        this._unIdentifiant = unIdentifiant;
        this._uneDesignation = uneDesignation;
        this._unNbmax = unNbmax;
    }
    
    get id() {
        return this._unIdentifiant;
    }

    get designation() {
        return this._uneDesignation;
    }

    get nbmax() {
        return this._unNbmax;
    }
}
module.exports = activite;