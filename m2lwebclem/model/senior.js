class senior {

    constructor(unNumSecu, unNom, uneDateNaissance, uneDateDeces, unEmail, unMDP){
        this._unNumSecu = unNumSecu;
        this._unNom = unNom;
        this._uneDateNaissance = uneDateNaissance;
        this._uneDateDeces = uneDateDeces;
        this._unEmail = unEmail;
        this._unMDP = unMDP;
    }


    get unNumSecu() {
        return this._unNumSecu;
    }

    get unNom() {
        return this._unNom;
    }

    get uneDateNaissance() {
        return this._uneDateNaissance;
    }

    get uneDateDeces() {
        return this._uneDateDeces;
    }

    get unEmail() {
        return this._unEmail;
    }

    get unMDP() {
        return this._unMDP;
    }
}
module.exports = senior;