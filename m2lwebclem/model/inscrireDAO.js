const { Client } = require('pg');

class inscrireDAO{

    constructor(){
    }

    updateLesActivite(numA,numS,cb){
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        });

        client.connect(function (err) {
            if (err) return done(err);
        });

        const query = {
            name: 'insert',
            text: 'INSERT INTO participer VALUES ($1,$2)',
            values: [numS,numA]
        };

        client.query(query, function (err) {
            if (err) {
                console.log(err.stack);
            } else {
                cb();
            }
            client.end();
        });
    };

    deleteLesActivite(numA,numS,cb){
        let client = new Client({
            connectionString : 'postgres://c7585:c7585@192.168.222.86:5432/M2L'
        });

        client.connect(function (err) {
            if (err) return done(err);
        });

        const query = {
            name: 'insert',
            text: 'DELETE FROM participer where participer.codesenior = $1 AND participer.codeactivite = $2',
            values: [numS,numA]
        };

        client.query(query, function (err) {
            if (err) {
                console.log(err.stack);
            } else {
                cb();
            }
            client.end();
        });
    };
}
module.exports = inscrireDAO;