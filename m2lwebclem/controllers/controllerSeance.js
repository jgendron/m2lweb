const pg = require('pg');
const ActivitePgDAO = require('../model/activitePgDAO');

const activitePgDAO = new ActivitePgDAO();

const SeancePgDAO = require('../model/seancePgDAO');

const seancePgDAO = new SeancePgDAO();


exports.getActivitesBySenior=function(req, res, next) {
    activitePgDAO.getActivitesBySenior(req.params.num,
        function(lesActivites){

            res.render('activiteInscrit',{listeDesActivites: lesActivites})

        }
    );
};

exports.getSeanceByActivite=function(req, res, next) {
    seancePgDAO.getSeanceByActivite(req.params.num,
        function(lesSeances){

            res.render('seances',{listeDesSeances: lesSeances})


        }
    );
};

