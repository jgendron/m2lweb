const pg = require('pg');

const ActivitePgDAO = require('../model/activitePgDAO');
const activitePgDAO = new ActivitePgDAO();

exports.getInfoProg = function(req, res) {
    activitePgDAO.getActivitesBySenior(req.session.user['numsecu'],
        function (lesActivites){
            activitePgDAO.getActivitesDispoBySenior(req.session.user['numsecu'],
                function (lesActivitesDispo){
                    res.render('programmeSante',{listeDesActivitesDispo: lesActivitesDispo,listeDesActivites: lesActivites})
                }
            );
        }
    );
};

const InscrireDAO = require('../model/inscrireDAO');
const inscrireDAO = new InscrireDAO();

exports.inscriptionAct = function(req, res){
    inscrireDAO.updateLesActivite(req.body.inscrit,req.session.user['numsecu'],
        function(){
            res.render('inscrire',{activite: req.body.inscrit});
        }
    )

};

exports.retractAct = function(req, res){
    inscrireDAO.deleteLesActivite(req.body.retracter,req.session.user['numsecu'],
        function(){
            res.render('retracter',{activite: req.body.retracter});
        }
    )
};