const pg = require('pg');

const ActivitePgDAO = require('../model/activitePgDAO');
const activitePgDAO = new ActivitePgDAO();


/* get des activites pour le bouton "CONSULTER" get ActivitesBySenior disponible aussi dans controllerProfile pour le bouton "Mon profil" */

exports.getActivitesBySenior=function(req, res, next) {
    activitePgDAO.getActivitesBySenior(req.session.user['numsecu'],
        function(lesActivites){

            res.render('profile',{listeDesActivites: lesActivites})

        }
    );
};

exports.getAllActivites=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://c7585:c7585@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite',
            text: 'SELECT * FROM activite'
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('activite', {listeDesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};
