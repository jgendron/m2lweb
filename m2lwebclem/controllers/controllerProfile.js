const pg = require('pg');

const SeniorDAO = require('../model/seniorDAO');
const seniorDAO = new SeniorDAO();

const ActivitePgDAO = require('../model/activitePgDAO');
const activitePgDAO = new ActivitePgDAO();

exports.profileSeniorInfo = function (req, res){

    seniorDAO.getSeniorActif(req.session.user['numsecu'],
        function (senior){

            /* get des activites pour le bouton "Mon profil" get ActivitesBySenior disponible aussi dans controllerProfile pour le bouton "CONSULTER" */

            activitePgDAO.getActivitesBySenior(req.session.user['numsecu'],
                function (lesActivites){

                    res.render('profile',{listeDesActivites: lesActivites,listeSenior: senior})

                }
            );
        }
    );
};